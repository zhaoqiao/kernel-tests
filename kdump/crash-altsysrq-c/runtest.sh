#!/bin/sh
# Copyright (c) 2020 Red Hat, Inc. All rights reserved.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Source Kdump tests common functions.
. ../include/runtest.sh

LoadAltSysRqDriver()
{
    Log "Build altsysrq module"
    which gcc > /dev/null 2>&1 || InstallDevTools
    MakeModule altsysrq
    LogRun "insmod altsysrq/altsysrq.ko" || MajorError 'Unable to load altsysrq.ko'
    LogRun "lsmod | grep altsysrq"
    LogRun "echo 1 > /proc/sys/kernel/sysrq"
}

TriggerAltSysrqC()
{
    Log "Trigger altsysrq-c panic"
    sync;sync;sync; sleep 10
    echo c > /proc/driver/altsysrq
}

#+---------------------------+

Multihost SystemCrashTest TriggerAltSysrqC LoadAltSysRqDriver
