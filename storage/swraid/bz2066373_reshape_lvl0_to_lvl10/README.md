# storage/swraid/bz2066373_reshape_lvl0_to_lvl10

Storage: Check if mdadm reshape level 0 to level 10

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
