# storage/nvme_rdma/hardtarget/nvmeof_rdma_connect_disconnect

Storage: nvmeof rdma connect disconnect stress testing with NETAPP E5700

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
