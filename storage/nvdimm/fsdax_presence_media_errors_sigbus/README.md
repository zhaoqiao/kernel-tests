# storage/nvdimm/fsdax_presence_media_errors_sigbus

Storage: nvdimm feature test fo enable DAX in the presence of media errors sigbus, from BZ1383825

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
